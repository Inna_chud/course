<?php

$str = '1 2 3 4 5';
$arr = explode(' ', $str);
//var_dump($arr);

$arr = array('a', 'b', 'c');
$string = implode(', ', $arr);
///var_dump($string);

$str = 'Ivan';
$age = 34;
printf("Hello, %s %d old \n", $str, $age);
// Hello, Ivan 34 old;

echo "Hello $str $age old \n";

echo 'Hello ' . $str .' '. $age .' old'.PHP_EOL;

$arr = array('name' => 'Ivan');
printf('Hello, %s', $arr['name']);

$str = sprintf('Hello, %s', $arr['name']);
echo $str.PHP_EOL;


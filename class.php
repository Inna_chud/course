<?php

class Man {
    
    public $age;
    public $weight = 60;
    
    protected $colorEye;
    
    private $maxSpeed;
    
    public function __construct($age, $weight) {
        $this->age = $age;
        $this->weight = $weight;
    }
    
    public function setColorEye($color) {
        if ($color == 'blue' || $color == 'green') {
            $this->colorEye = $color;
        }
        // $this == $obj когда $obj->setColorEye
    }
    
    public function setMaxSpeed($maxSpeed) {
        $this->maxSpeed = $maxSpeed;
    }
            
}

$obj = new Man(10, 80);
var_dump($obj);
//var_dump($obj->age);
//var_dump($obj->weight);
////var_dump($obj->colorEye); // нельзя! 
//$obj->setColorEye('blue');
//$obj = new Man;

/*
NULL
int(60)
object(Man)#1 (4) {
  ["age"]=>
  NULL
  ["weight"]=>
  int(60)
  ["colorEye":protected]=>
  NULL
  ["maxSpeed":"Man":private]=>
  NULL
}
 */


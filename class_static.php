<?php

//class Phone {
//    
//    public $number = 123;
//    
//    public static $value = 123;
//    
//    public function test() {
//        self::$value++;
//        
//        $this->number++;
//        
//        // Phone::testStatic();
//    }
//    
//    public static function testStatic() {
//        echo 'static call testStatic';
//    }
//    
//}
//
//$obj = new Phone();
/////$obj->test();
//
//$obj1 = new Phone();
////$obj1->test();
//
////Phone::testStatic();
//
////echo Phone::$value . PHP_EOL;
//
////include("include1.php");
//
//function login($login, $password) {
//    if ($login == Setting::$login && $password == Setting::$password) {
//        echo 'ok!';
//    }
//}
////login('1', '2');
//
//// Singleton
//
//class App {
//    
//    public static $db1;
//    public static $db2;
//    public static $db3;
//    
//    public static function test() {
//        //self::
//        //parent::
//    }
//    
//}
//
//App::$db1 = 1; // подключение к базе 1
//App::$db2 = 2; // подключение к базе 1
//App::$db3 = 3; // подключение к базе 1
//App::test();
//
//class User {
//    
//    public function save() {
//        App::$db1->save();
//    }
//    
//}

// Наследование

class Validator {
    
    public function isEmail($email) {
        // проверка того, что в $email есть @
        return true;
    }
    
}

class SuperValidator extends Validator {
    
    public function isEmail($email) {
        if (parent::isEmail($email)) {
            // проверку того, что в $email есть .
            return true;
        } else {
            return false;
        }
    }
    
}

$obj = new Text();
$obj->test();


<?php

//$a = 'foo';
//
//function foo() {
//    echo 'Hello' . PHP_EOL;
//}
//
//foo();
//$a(); // foo()

function get_name() {
    return 'Aleks';
}

function get_lastname() {
    return 'Ivanov';
}

$arr = array(
    'name'      => null,
    'lastname'  => null
);

foreach ($arr as $ind => &$val) {
    $funcName = 'get_'.$ind;
    $val = $funcName();
}
unset($val);

$val = 123;

//var_dump($arr);

function getID() {
    static $c = 0;
    return $c++;
}

var_dump(getID());
var_dump(getID());
var_dump(getID());
var_dump(getID());


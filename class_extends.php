<?php

abstract class Animal {
    
    public $name;
    public $country;
            
    public function __construct($name, $country) {
        $this->name = $name;
        $this->country = $country;
    }
}

// наследоваться может только один класс
class Bird extends Animal {
    
    public $timeUp;
    
    public function __construct($name, $country, $timeUp) {
        parent::__construct($name, $country);
        $this->timeUp = $timeUp;
    }
}

class Snake extends Animal {
    
    public $length;
    
    public function __construct($name, $country, $length) {
        parent::__construct($name, $country);
        $this->length = $length;
    }
    
}

$bird = new Bird('Синица', 'Россия', 60);
$snake = new Snake('Удав', 'Далеко )', 30);

var_dump($bird, $snake);

//$text = 'name';
//$obj->$text; // $obj->name
<?php

/* 
* Создать класс MobileOperator
*	свойства
*		protected $phones // массив из объектов класса Phone
*	методы
*		public addPhone($number) // принимает номер телефона, создает объект класса Phone и добавляет его в массив $this->phones
*		public findByNumber($number) // метод поиска объекта в массиве $this->phones по номеру телефона (foreach по $this->phones as $phone и если найден такой $phone->getNumber() == $number то return $phone, иначе return false)
*
*Создать класс Phone
*	свойства
*		protected $mobileOperator; // объект класса MobileOperator
*		protected $number;
*	методы: 
*		public call($number) // позвонить
*		public accept($phone) // принять вызов. $phone объект класса Phone
*		public getNumber() // возвращает номер телефона
*		public setNumber($number) // устанавливает номер телефона
*В конструкторе Phone принимать аргумент $mobileOperator и $number и устанавливать 
*в свойства $this->mobileOperator и $this->number(через метод $this->setNumber)
*
*Создать массив $numbers и сразу заполнить его несколькими номерами телефонов
*Создать объект $megafon класса MobileOperator и в цикле foreach по $numbers вызывать 
*у $megafon метод addPhone для добавления телефонов
*
*Теперь о том, как должны работать методы call и accept в классе Phone:
*	Метод call($number)
*		Выполняет метод $phone = $this->mobileOperator->findByNumber($number) 
*               и либо получает объект класса Phone, либо false
*		Если объект найден, то делаем $phone->accept()
*		Если не найден (false), то пишем 'Телефон такой-то не найден'
*
*	Метод accept($phone)
*		Просто выводит текст 'Поступил входящий звонок с номера ' . $phone->getNumber()
*
*Теперь все готово к тому, чтобы запустить и проверить. Для этого пишем код, выполнящий следующее действие
*Находим объект класса Phone используя один из имеющихся телефонов (любой) через
* $megafon->findByNumber(номер телефона) 
* и помещаем в любую переменную, к примеру $myPhone. У этой переменной вызываем 
* метод $myPhone->call(тут передаем номер телефона, который так же есть в списке)
* и получаем информацию о том, что поступил входящий звонок.
*После этого так же делаем $myPhone->call(и тут любой номер, которого нет в списке) 
* и получаем информацию о том, что телефон такой-то не найден
*/

Class MobileOperator {

    protected $phones = array(); // массив из объектов класса Phone 

    public function addPhone($number) {
        // принимает номер телефона, создает объект класса Phone и добавляет его в массив $this->phones
        $this->phones[]=new Phone($this, $number);        
    }

    public function findByNumber($number) {
        //метод поиска объекта в массиве $this->phones по номеру телефона 
        //(foreach по $this->phones as $phone и если найден такой $phone->getNumber() == $number,
        //то return $phone, иначе return false)
        
        foreach ($this->phones as $phones_cur) {
            $number_cur = $phones_cur->getNumber();            
            if ($number_cur == $number) {
                return $phones_cur;
            }
        }        
        return new Phone($this,null);
    }

}
Class Phone {

    protected $mobileOperator;
    protected $number;

    public function __construct($mobileOperator, $number) {
        $this->mobileOperator = $mobileOperator;
        $this->setNumber($number);        
    }
    
    public function call($number) {
        // позвонить
        $phone = $this->mobileOperator->findByNumber($number);        
        if ($phone->getNumber()==$number) {
            return $phone->accept($number);
        } else {
            //echo 'Телефон '.$number.' не найден' . PHP_EOL;
            return array('result'=>false, 'msg'=>"Номер телефона $number не найден");
        }
    }

    public function accept($number) {
    // принять вызов. $phone объект класса Phone
        //echo 'Поступил входящий звонок с номера ' . $phone->getNumber().PHP_EOL;
        return array('result'=>true, 'msg'=>"Номер телефона $number найден");
    }

    public function getNumber() {
    // возвращает номер телефона
    return $this->number;
    }

    public function setNumber($number) {
    // устанавливает номер телефона
    $this->number=$number;
    }

}







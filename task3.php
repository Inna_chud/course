<?php

/* 
 Первая функция возвращает объект

 Вторая функция меняет объект
 */

// вариант возврата значения
function fo1($arg1,$arg2,$arg3) {
   $obj = (object)array($arg1,$arg2,$arg3);
   return $obj;
}
$b=fo1('Чудиновских','Инна',1976);
var_dump($b);

// изменение по ссылки
function fo($obj,$arg1,$arg2,$arg3){ 
    $obj->first= $arg1; 
    $obj->last = $arg2;
    $obj->year = $arg3;
}

$abc = (object)array();
fo($abc,'Чудиновских','Инна',1976);           
var_dump($abc);


// Решение от преподавателя 
function init($name, $lastName, $year) {
    return (object)array(
        'name'      => $name,
        'last_name' => $lastName,
        'year'      => $year 
    );
}

$obj = init('Alex', 'Ivanov', 1234);
var_dump($obj);
/*
object(stdClass)#1 (3) {
  ["name"]=>
  string(4) "Alex"
  ["last_name"]=>
  string(6) "Ivanov"
  ["year"]=>
  int(1234)
}
 */

function set($o, $name, $lastName, $year) {
    $o->name = $name;
    $o->last_name = $lastName;
    $o->year = $year;
}

set($obj, 'Petr', 'Petka', 3333);
var_dump($obj);
/*
object(stdClass)#1 (3) {
  ["name"]=>
  string(4) "Petr"
  ["last_name"]=>
  string(5) "Petka"
  ["year"]=>
  int(3333)
}
 */
<?php

    $arr = array(
        'text1' => 'b', 
        'text2' => 'c',
        'text3' => 'a'
    );
    
    // #1
    foreach ($arr as $name) {
        echo $name;
    }
    
    // #2
    foreach ($arr as $i => $name) {
        echo $i . ' = ' . $name;
    }
    
    echo 123;
    echo PHP_EOL;
    
    $i = 1;
    while ($i < 5) {
        echo $i;
        $i++;
    }
    // $i == 5
    
    do {
        echo 1;
    } while ($i != 5);
    
    // 12341
<?php

interface IDataBase {
    
    public function getDBConection();
    public function save($data);
    public function get($key);
    
}

class DBMysql implements IDataBase {
    
    public function getDBConection() {
        return 123;
    }
    
    public function save($data) {
        // save data to mysql
    }
    
    public function get($key) {
        // get from mysql;
        return 12345;
    }
    
}

class DBMSSql implements IDataBase {
    
    public function getDBConection() {
        return 'efwef';
    }
    
    public function save($data) {
        // save data to mssql
    }
    
    public function get($key) {
        // get from mssql;
        return 'efe';
    }
    
}

$obj = new DBMSSql();
$obj->save(array(13));

interface Phone {
    
    public function call($number);
    public function sms($number, $text);
    
}

interface TouchDisplay {
    
    public function touch($x, $y);
    
}

class Nokia implements Phone {
    
    public function call($number) {
        
    }
    
    public function sms($number, $text) {
        
    }
    
}

class iPhone implements Phone, TouchDisplay {
    
    public function call($number) {
        
    }
    
    public function sms($number, $text) {
        
    }
    
    public function touch($x, $y) {
        
    }
    
}


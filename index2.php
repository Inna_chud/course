<?php

//include "include1.php";
//include "include1.php";
//include "include1.php";
//include "include1.php";


//include_once "include1.php";
//include_once "include1.php";
//include_once "include1.php";
//include_once "include1.php";

//require "include1.php";
////require "include1.php";
////require "include1.php";
////require "include1.php";
//

//require_once "include1.php";
////require_once "include1.php";
////require_once "include1.php";
////require_once "include1.php";
////require_once "include1.php";
//echo '123'.PHP_EOL;

//PHP Notice
//PHP Warning
//PHP Fatal

$config = include "include1.php";
if ($config['login'] == 'admin' && $config['password'] == '123') {
    echo 'super' . PHP_EOL;
}

// объявление
//function foo1($need, $temporary = 1) {
//    echo $need . $temporary;
//}
//
//// вызов
//foo('me', 2); // me2
//foo('ww'); // ww1
//foo('vv');
//
//$a = 'asd';
//
//$b = $a;
//$b = 123;
//echo $a; // 'asd'
//
//// ссылка
//$b = &$a;
//$b = 123;
//echo $a; // 123;
//$a = 'ggg';
//echo $b; // 'ggg';


//// вариант с возвращением результата
//function foo($need) {
//    $need = 123;
//    return $need;
//}
//
//$a = 'abc';
//$a = foo($a);
//echo $a; // 123
//
//// вариант с изменением по ссылке
//function fooN(&$need) {
//    $need = 123;
//}
//$a = 'abc';
//fooN($a);
//echo $a; // 123
//
//// вариант возврата ссылки
//function &fooR(&$arg) {
//    return $arg;
//}
//
//$b = 123;
//$a = &fooR($b); // $a = &$b;
//$a = 'abc';
//echo $b; // 'abc'
//
//// область видимости
//$a = 123;
//function abc() {
//    global $a;
//    $a = 'abc';
//}
//abc();
//echo $a; // abc

// обращение к переменным
$test = 'hello';
$hello = 123;

$$test++; // $hello++

$a = 0;
$b = 0;
$c = 0;

$arr = array('a', 'b', 'c');
foreach ($arr as $value) {
    $$value++;
}

/*
 * $a == 1
 * $b == 1
 * $c == 1
 */

function foo() { }
foo();

$func = function () { };
$func();

$a = 'foo';
$a();


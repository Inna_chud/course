<?php

$str = '123';
//var_dump($str);

$int = (int)$str;
//var_dump($int);
//
//var_dump((string)$int);
//
//var_dump((bool)$int);

/*
string(3) "123"
int(123)
string(3) "123"
bool(true)
 */

//0, null, array(), '' -> (bool) -> false

$arr = array('key' => 'value');
$arr['key'];
$arr[] = 123;
//var_dump($arr);

$obj = (object)$arr; // (object)array()
$obj->key;
$obj->name = 123;
//var_dump($obj);

$about = (object)array(
    'first_name' => null,
    'last_name' => null,
    'year' => null
);
$about->first_name;
$about->last_name;
//...

$arr = array(123);
$b = &$arr;
$b = null; // $arr == null;

$info = $about;
$info->first_name = 'Alex'; // $about->first_name == 'Alex'

function setFirstName($data) {
    $data->first_name = 'Alex';
}
foo($about);

// Объекты всегда передаются по ссылке
$newAbout = clone $about;


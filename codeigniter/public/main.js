var formLogin = {
    onLoad: false,
    s: {
        btnLogin: '#btn_login',
        inputPassword: '#password',
        inputLogin: '#login',
        label: '#label'
    },
    init: function () {
        $(this.s.btnLogin).click(this.login.bind(this));
    },
    login: function () {
        var login = $(this.s.inputLogin).val(),
            password = $(this.s.inputPassword).val();

        if (this.onLoad) {
            return;
        }

        this.onLoad = true;
        $(this.s.btnLogin).toggle();
        $(this.s.label).text('Идет загрузка').show();

        $.post(
                '/index.php/login',
                {
                    login: login,
                    password: password
                },
        this.afterLogin.bind(this)
                );
    },
    afterLogin: function (data) {
        this.onLoad = false;
        if (data == 'not found') {
            $(this.s.label).toggleClass('label-warning')
                    .toggleClass('label-default')
                    .text('Неверный логин или пароль');

            return this.resetForm();
        } else if (data == 'success') {            
            window.location = '/index.php/messages/';
        } 
        else {
            $(this.s.label).toggleClass('label-warning')
                    .toggleClass('label-default')
                    .text('Неверный ответ от сервера');

            return this.resetForm();
        }
    },
    resetForm: function () {
        setTimeout(function () {
            $(this.s.label).toggle()
                    .toggleClass('label-default')
                    .toggleClass('label-warning');
            $(this.s.btnLogin).toggle();
        }.bind(this), 2000);
    }
}
var formText = {
     onLoad: false,
    
    s: {
        btnMessages: '#btn_messages',
        inputId:     '#id',
        inputText:   '#text',
        label:       '#label'
    },
    
    init: function() {
        $(this.s.btnMessages).click(this.messages.bind(this));
    },
    
    messages: function() {
        var id = $(this.s.inputId).val(),
            text = $(this.s.inputText).val();
    
        if (this.onLoad) {
            return;
        }
        
        this.onLoad = true;
        $(this.s.btnMessages).toggle();
        $(this.s.label).text('Идет загрузка').show();
    
        $.post(
            '/index.php/messages',
            {
                id: id,
                text:text 
            },
            this.afterMessages.bind(this)
        );
    },
    
    afterMessages: function(data) {        
        this.onLoad = false;      
        if (data == 'it is not added') {
            $(this.s.label).toggleClass('label-warning')
                           .toggleClass('label-default')
                           .text('Ошибка ввода');
                   
            return this.resetMessages();
        } else if (data == 'success') {                
            window.location = '/index.php/messages/';
        } else {
            $(this.s.label).toggleClass('label-warning')
                           .toggleClass('label-default')
                           .text('Неверный ответ от сервера');
                   
            return this.resetMessages();
        }                
    },
    
    resetMessages: function() {
        setTimeout(function() {
            $(this.s.label).toggle()
                    .toggleClass('label-default')
                    .toggleClass('label-warning');
            $(this.s.btnMessages).toggle();
        }.bind(this), 2000);
    }
}
var formRegistration = {
     onLoad: false,
    
    s: {
        btnRegitration:  '#btn_registration',
        inputId:         '#id',
        inputLogin:      '#login',
        inputPassword:   '#password',
        inputFirst_name: '#first_name',
        inputLast_name:  '#last_name',
        label:           '#label'
    },
    
    init: function() {        
        $(this.s.btnRegitration).click(this.registration.bind(this));
    },
    
    registration: function() {
        var id = $(this.s.inputId).val(),    
            login=$(this.s.inputLogin).val(),
            password=$(this.s.inputPassword).val(),
            first_name=$(this.s.inputFirst_name).val(),
            last_name=$(this.s.inputLast_name).val();                   
    
        if (this.onLoad) {
            return;
        }
        
        this.onLoad = true;
        $(this.s.btnRegistration).toggle();
        $(this.s.label).text('Идет загрузка').show();
           
        $.post(
            '/index.php/registration',
            {
                id: id,    
                login:login,
                password:password,
                first_name:first_name,
                last_name:last_name
            },
            this.afterRegistration.bind(this)
        );
    },
    
    afterRegistration: function(data) {
        console.log(data);
        this.onLoad = false;              
        if (data == 'it is not registered') {
            $(this.s.label).toggleClass('label-warning')
                           .toggleClass('label-default')
                           .text('Ошибка ввода');
                   
            return this.resetRegistration();
        } else if (data == 'success') {
            console.log(1);
            window.location = '/index.php/messages/';
        } else {
            $(this.s.label).toggleClass('label-warning')
                           .toggleClass('label-default')
                           .text('Неверный ответ от сервера');
                   
            return this.resetRegistration();
        }                
    },
    
    resetRegistration: function() {
        setTimeout(function() {
            $(this.s.label).toggle()
                    .toggleClass('label-default')
                    .toggleClass('label-warning');
            $(this.s.btnRegistration).toggle();
        }.bind(this), 2000);
    }
}
var removeText = {
    onLoad: false,    
    id_btn: 0,
    id_label: 0,
    id_message: 0,
    
    s: {        
        btnRemove: '.btn_remove', 
        label: '.label_remove',
        message: '.message'
    },
              
    init: function () {            
        $(this.s.btnRemove).click(function(){
            this.id_btn=$(this),        
            this.remove_message.bind(this);
        });        
    },
    
    remove_message: function () {      
        var id = this.id_btn.data('id')
        this.id_label=$(this.s.label).filter(function( index ) {                      
                      return $(this).data('id')==id;
                      });               
        this.id_message=$(this.s.message).filter(function( index ) {                      
                      return $(this).data('id')==id;
                      });     
        if (this.onLoad) {
            return;
        };

        this.onLoad = true;
        this.id_btn.toggle();
        this.id_label.text('Идет удаление').show();

        $.get(
                '/index.php/messages/remove',
                {
                    id: id
                },
        this.afterRemove.bind(this)
                );
    },
    afterRemove: function (data) {
        this.onLoad = false;
        if (data == 'success') {            
            this.id_message.remove();
            //window.location = '/index.php/messages/';
        } else {
            this.id_label.toggleClass('label-warning')
                    .toggleClass('label-default')
                    .text('Неверный ответ от сервера');
            return this.resetRemove();
        }
    },
    resetRemove: function () {
        setTimeout(function () {
            this.id_label.toggle()
                    .toggleClass('label-default')
                    .toggleClass('label-warning');            
            this.id_btn.toggle();
        }.bind(this), 2000);
    }              
}
# create table test (
#    id INT NOT NULL AUTO_INCREMENT,
#    user_id INT NOT NULL,
#    text VARCHAR(50),
#    PRIMARY KEY (id)
# );

# alter table test add column `time` int default 0 after id;

# alter table test add column `time1` int default 0 first;

# alter table test drop column time1;

# alter table users modify column id int not null auto_increment;
# select * from users;

# show create table test;

/* "CREATE TABLE `test` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `time` int(11) DEFAULT '0',
  `user_id` int(11) NOT NULL,
  `text` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci" */

# desc test;

# insert into test (id, time, user_id, text) values (null, 1, 1, 'abc');

# insert into test (time, user_id, text) values (1, 1, 'abc');
# select * from test;

#create table comments (
#    id INT NOT NULL AUTO_INCREMENT,
#    message_id INT NOT NULL,
#    user_id INT NOT NULL,
#    created INT NOT NULL,
#    PRIMARY KEY (id)
# );

# desc comments;
# insert into comments (message_id, user_id, created) values (1, 1, 12);
# select * from comments;

# insert into test (time, user_id, text) values (1, 20, 'abc');

# без индекса
# explain select * from test where user_id = 20;
# 1	SIMPLE	test	ALL	""	""	""	""	12	Using where

# create index `for_search` on test (user_id);
# create index `text_index` on test (text);
# create index `double_index` on test (user_id, text);
# drop index `double_index` on test;

# explain select * from test where user_id = 1;

# explain select * from test where id = 8;

# explain select * from test where user_id = 2 and text = 'abc2';

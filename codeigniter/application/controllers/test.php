<?php

class Test extends CI_Controller {
    
    public function index() {
        $this->load->view('test');
    }    
    
    public function ajax() {
        $this->load->view('ajax', $_GET);
    }
    
}
 
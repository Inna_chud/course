<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Messages extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('Users_Model');
        $this->load->model('Messages_Model');

        if (!$this->user) {
            $this->redirect('/index.php');
        }
    }
    
    public function index() {        
        $error = '';
        if ($_POST && $_POST['text']) {            
            
            if (!Messages_Model::getForId($_POST['id'])) {                
                // Просмотр данныхе в таблице messages
                Messages_Model::add($_POST['id'], $this->user->id, $_POST['text']);
                // todo: временно для локального тестирования
                sleep(1);
                echo 'success'; 
                die();
            } else {
                $error = 'Сообщение c таким Id существует';
                // todo: временно для локального тестирования
                sleep(1);
                echo 'it is not added';
                die();
            }
        }

        // Отобразить таблицу messages
        $obj = new Messages_Model();
        $list = $obj->getListId($this->user->id);

        $this->render('messages', array(
            'list' => $list,
            'error' => $error
        ));
    }
    
    public function remove() {
        $id = isset($_GET['id']) ? $_GET['id'] : null;
        Messages_Model::remove($id);
        // todo: временно для локального тестирования
        sleep(1);
        echo 'success';
        die();
        //header('location: /index.php');
    }
    
    public function edit() {
        $id = isset($_GET['id']) ? $_GET['id'] : null;

        $error = '';
        if ($_POST) {
            $id = $_POST['id'];
            if ($_POST['text']) {
                Messages_Model::save($id, $_POST['text']);
                $this->redirect('/index.php/messages');
            } else {
                $error = 'Введите сообщение';
            }
        }

        $res = Messages_Model::getText($id);
        $this->render('messages_edit', array(
            'id' => $res->id,
            'msg' => $res->text,
            'error' => $error
        ));
    }

    public function send() {       
        
        if ($_POST){
            
            if ($_POST['text']) {                
                $id = Messages_Model::getIdMax();                                
                $id = $id ? $id+1 : 1;                  
                
                Messages_Model::add($id, $_POST['user_id'], $_POST['text']);
                $this->redirect('/index.php/messages');
            } else {
                
                $error = 'Введите сообщение';
            }
        }

        $obj = new Users_Model();        
        $list = $obj->getList();

        $this->render('messages_send', array(
            'list' => $list,
            'error' => isset($error) ? $error : null
        ));
    }

}

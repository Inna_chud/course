<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Registration extends MY_Controller {

    public function __construct() {
        parent::__construct();

        $this->load->model('Users_Model');
    }

    public function index() {                
        if ($this->user) {
            setcookie('user_id', 0, 0, '/');                    
        }
        $this->user=new Users_Model();
        
        $error = '';
        if ($_POST) {            
            foreach ($_POST as $field => $value) {                
                if (!$value) {                    
                    $error = array(
                        'error' => 1,
                        'msg' => 'Введите все данные'
                    );
                    break;
                } elseif ($field == 'password') {
                    $this->user->$field = md5($value);
                } else {
                    $this->user->$field = $value;
                }
            }               
            if (!$error) {                                
                if (!Users_Model::getForId($this->user->id)) {

                    if (!Users_Model::getForLogin($this->user->login)) {
                        Users_Model::add($this->user);                        
                        setcookie('user_id', $this->user->id, 0, '/');
                        // todo: временно для локального тестирования
                        sleep(1);           
                        echo 'success';
                        die();
                        //$this->redirect('/index.php/messages');
                    } else {
                        $error = array(
                            'error' => 1,
                            'msg' => 'Пользователь c таким Login существует');
                        // todo: временно для локального тестирования
                        sleep(1);           
                        echo 'it is not registered';
                        die();
                    }
                } else {
                    $error = array(
                        'error' => 1,
                        'msg' => 'Пользователь c таким Id существует');
                    // todo: временно для локального тестирования
                    sleep(1);           
                    echo 'it is not registered';
                    die();
                }
            } else {
              // todo: временно для локального тестирования
              sleep(1);             
              echo 'it is not registered';
              die();  
            }
        }

        $this->render('registration', array(
            'user' => $this->user,
            'error' => $error
        ));
    }

}

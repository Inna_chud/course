<?php

class Users_Model extends CI_Model {
    
    public $id;
    public $login;
    public $password;
    public $first_name;
    public $last_name;
        
    public function getList() {
        $query = $this->db->get('users');
        return $query->result();
    }
    
    public static function save($user) {
        $obj = new self;
        $res = $obj->db->update('users', $user, array('id' => $user->id));
    }
    
    public static function add($user) {
        $obj = new self;
        $res = $obj->db->insert('users', $user);
    }
    
    public static function getForId($id) {
        $obj = new self;
        $query = $obj->db->get_where('users', array('id' => $id));
        if ($res = $query->result()) {
            return $res[0];
        } else {
            return false;
        }
    }
    
    public static function getForLogin($login) {
        $obj = new self;
        $query = $obj->db->get_where('users', array('login' => $login));
        if ($res = $query->result()) {
            return $res[0];
        } else {
            return false;
        }
    }
    
    public static function remove($id) {
        if (!$id) {
            return false;
        }
        
        $obj = new self;
        $obj->db->delete('users', array('id'=>$id));
    }
    
    public static function make($id, $firstName, $lastName) {
        if (!$id || !$firstName || !$lastName) {
            return array(
                'error' => 1,
                'msg' => 'Введите все данные'
            );
        }
        
        $obj = new self;
        $obj->id = $id;
        $obj->first_name = $firstName;
        $obj->last_name = $lastName;
        
        $res = $obj->db->insert('users', $obj);
        
        return array('error'=>0);
    }
    
    /**
     * Получить объект пользователя для пары login и пароль
     * @param string $login
     * @param string $password
     * @return false|stdClass
     */
    public static function getForLoginPassword($login, $password) {
        $obj = new self;

        $query = $obj->db->get_where('users', array(
            'login' => $login,
            'password' => md5($password)
        ));
        $res = $query->result();        
        return $res ? $res[0] : false;
    }

}
<?php

class Messages_Model extends CI_Model {
    
    public $id;
    public $user_id;
    public $text;        
        
    public function getListId($id) {
        $query = $this->db->get_where('messages',array('user_id'=>$id));
        return $query->result();
    }
    
    public static function add($id,$user_id,$text){
        $obj= new self;
        $obj->id      = $id;
        $obj->user_id = $user_id;
        $obj->text    = $text;
        
        $res=$obj->db->insert('messages', $obj);
    }
    
    public static function getForId($id) {
        $obj = new self;
        $query = $obj->db->get_where('messages', array('id' => $id));
        if ($res = $query->result()) {
            return $res[0];
        } else {
            return false;
        }
    }
    
    public static function remove($id) {
        if (!$id) {
            return false;
        }
        
        $obj = new self;
        $obj->db->delete('messages', array('id'=>$id));
    }
    
    public static function save($id,$text) {
        $obj = new self;
        $res = $obj->db->update('messages', array('text'=>$text), array('id' => $id));
    }
    
    public static function getText($id) {
        $obj = new self;
        $query = $obj->db->get_where('messages',array('id' => $id));
        if ($res = $query->result()) {
            return $res[0];
        } else {
            return false;
        }
    }
    
    public static function getIdMax(){
        $obj = new self;
        //$query =$this->db->get('messages',array('max(id)'));        
        $query=$obj->db->query('SELECT max(id) as maxId FROM messages');
        $res = $query->result();       
        return $res ? $res[0]->maxId : 0;
    }

}
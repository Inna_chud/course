<div style="margin-bottom: 20px;">
    <a href="/index.php/messages/send">Отправить сообщение</a>
</div>
<table class="table table-striped table-bordered">
    <tr>
        <th>ID</th>        
        <th>TEXT</th>
        <th>x</th>
        <th>x</th>
    </tr>
    <?php foreach ($list as $msg): ?>
        <tr class="message" data-id=<?php echo $msg->id ?>>
            <td><?php echo $msg->id ?></td>            
            <td><?php echo $msg->text ?></td>   
            <td>                
                <span class="btn_remove" data-id=<?php echo $msg->id ?>>Удалить</span>
                <span class="label_remove" style="display:none" data-id=<?php echo $msg->id ?>></span>
            </td>
            <td>
                <a href="/index.php/messages/edit?id=<?php echo $msg->id ?>">
                    Редактирование
                </a>
            </td>
        </tr>        
    <?php endforeach ?>
</table>

<h4>Добавление сообщения</h4>        
<?php if ($error): ?>
<p class="form-control-static" style="color:red">Ошибка: <?php echo $error?></p>
<?php endif ?>
<div>    
    <p>ID:</p>
    <input id="id" type="text" class="form-control" name="id">    
    <p>Текст сообщения:</p>
    <input id="text" type="text" class="form-control" name="text">
    <br>
    <span id="btn_messages" class="btn btn-default" >Добавить</span>
    <span id="label" class="label label-default" style="display:none"></span>
</div>    


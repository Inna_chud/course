<h3>Авторизация</h3>

<?php if ($err): ?>
    <p style="color:red"><?php echo $err?></p>
<?php endif ?>
    
<div>
    <input id="login" type="text" placeholder="Login" name="login">
    <input id="password" type="password" placeholder="Password" name="password">
    <span id="btn_login" class="btn btn-default" >Войти</span>
    <span id="label" class="label label-default" style="display:none"></span>
</div>

<div style="margin-bottom: 20px;">
    <a href="/index.php/registration">Регистрация</a>
</div> 
    
<h4>Отправить сообщение пользователю</h4>
<?php if ($error): ?>
<p class="form-control-static" style="color:red">Ошибка: <?php echo $error?></p>
<?php endif ?>
<form action="/index.php/messages/send" method="post">
    <p>Кому:</p>
    <select class="form-control" name="user_id">
        <?php foreach ($list as $obj): ?>
        <option value="<?php echo $obj->id?>">
            <?php echo $obj->first_name.' '.$obj->last_name?>
        </option>
        <?php endforeach ?>
    </select>
    <p>Текст сообщения:</p>
    <input type="text" class="form-control" name="text">   
    <br>
    <button type="submit" class="btn btn-default">Send</button>
</form>


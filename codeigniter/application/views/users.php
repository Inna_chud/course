<div class="container" style="margin-bottom: 20px;">
    <a href="/index.php/edit">Изменить данные</a>
</div>

<table class="table table-striped table-bordered">
    <tr>
        <th>ID</th>
        <th>Имя</th>
        <th>Фамилия</th>
        <th>x</th>
    </tr>
    <?php foreach ($list as $user): ?>
        <tr>
            <td><?php echo $user->id ?></td>
            <td><?php echo $user->first_name ?></td>
            <td><?php echo $user->last_name ?></td>
            <td>
                <a href="/index.php/welcome/remove?id=<?php echo $user->id ?>">
                    Удалить
                </a>
            </td>                    
        </tr>
    <?php endforeach ?>
</table>

<form role="form" action="/index.php/welcome/test" method="post">
    <select name="user_id">
        <?php foreach ($list as $obj): ?>
        <option value="<?php echo $obj->id?>">
            <?php echo $obj->first_name.' '.$obj->last_name?>
        </option>
        <?php endforeach ?>
    </select>    
    <button type="submit" class="btn btn-default">Send</button>
</form>
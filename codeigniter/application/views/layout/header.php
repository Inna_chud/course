<!DOCTYPE html>
<html>
    <head>
        <title>Page hello</title>
        <link href="/public/dist/css/bootstrap.min.css" rel="stylesheet">                
        <script src="/public/jquery-2.1.1.min.js"></script>        
        <script src="/public/main.js"></script>       
        <script>
            <?php if (!$user): ?>
                $(function() {
                    formLogin.init();
                });
            <?php endif ?>
            $(function() {
                formText.init();
            });
            $(function() {
                formRegistration.init();
            });
            //$(function() {
            //    removeText.init();
            //});
        </script>
    </head>
    <body>
        <div class="container">
            <div style="margin-bottom: 20px; border-bottom: 1px solid silver">
                <?php if ($user): ?>
                    Логин: <?php echo $user->login?> 
                    <a href="/index.php/login/quit">выход</a>
                <?php else: ?>
                    Пользователь не авторизован
                <?php endif ?>
            </div>
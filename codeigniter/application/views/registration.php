<h3>Регистрация пользователя</h3>

<?php if ($error && $error['error']): ?>
<p class="form-control-static" style="color:red">Ошибка: <?php echo $error['msg']?></p>
<?php endif ?>
<div>    
    <input id="id" type="text" class="form-control" name="user[id]" placeholder="Id" value="<?php echo $user->id?>">    
    <input id="login" type="text" class="form-control" name="user[login]" placeholder="Login" value="<?php echo $user->login?>">    
    <input id="password" type="password" class="form-control" name="user[password]" placeholder="Password">
    <input id="first_name" type="text" class="form-control" name="user[first_name]" placeholder="Имя" value="<?php echo $user->first_name?>">
    <input id="last_name" type="text" class="form-control" name="user[last_name]" placeholder="Фамилия" value="<?php echo $user->last_name?>">
    <br>        
    <span id="btn_registration" class="btn btn-default" >Сохранить</span>
    <span id="label" class="label label-default" style="display:none"></span>
    <a href="/index.php">Отмена</a>
</div>


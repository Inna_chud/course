<h4>Редактирование сообщения</h4>

<?php if ($error): ?>
<p class="form-control-static" style="color:red">Ошибка: <?php echo $error?></p>
<?php endif ?>
<form action="/index.php/messages/edit" method="post">  
     <p>ID:</p>
    <input type="text" class="form-control" name="id" value="<?php echo $id?>">
    <p>Текст сообщения:</p>
    <input type="text" class="form-control" name="text" value="<?php echo $msg?>">        
    <br>
    <button type="submit" class="btn btn-default">Сохранить</button>
    <a href="/index.php/messages">Отмена</a>
</form>


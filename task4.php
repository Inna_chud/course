<?php

/* 
 Создать класс у которого есть свойства name, last_name и age
name и last_name через конструктор
protected age
setAge и getAge
 */
class Man {    
    public $name; 
    public $last_name;
    protected $age;
    
    public function __construct($name1, $last_name1) {
        $this->name = $name1;
        $this->last_name = $last_name1;
    }
    public function setAge($age1) {
        $this->age = $age1;
    }
    
    public function getAge() {
        return $this->age;
    }
}
$obj = new Man('Чудиновских','Инна');
$obj->setAge(100);
var_dump($obj);
var_dump($obj->getAge());